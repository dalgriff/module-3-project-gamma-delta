"""
Entry point for the FastAPI Application
"""
from fastapi import FastAPI,HTTPException
from fastapi.middleware.cors import CORSMiddleware
from routers import auth_router
import os
import random
 
app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[os.environ.get("CORS_HOST", "http://localhost:5173")],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(auth_router.router)


def generate_sample_data():
    return [random.randint(1, 100) for _ in range(10)]

@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00"
        },
        "sort_data": generate_sample_data()  # Adding sample data for sorting
    }


@app.get("/data/{dataset_type}")
def get_data(dataset_type: str):
    """ Endpoint to generate different types of datasets based on the input 
    parameter.
      """
    if dataset_type == 'randomNumbers':
        # Generate a list of 20 random numbers
        return [random.randint(1, 100) for _ in range(20)]
    elif dataset_type == 'sortedNumbers':
        # Generate a sorted list of 20 numbers
        return list(range(1, 21))
    elif dataset_type == 'reverseNumbers':
        # Generate a reverse sorted list of 20 numbers
        return list(range(20, 0, -1))
    else:
        # If the dataset type is unknown, return an HTTP 404 error
        raise HTTPException(status_code=404, detail="Dataset type not found")
    