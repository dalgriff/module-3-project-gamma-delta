// Bubble Sort Generator
export function* bubbleSort(data) {
    let arr = [...data];
    let swapped;
    console.log("Starting Bubble Sort: ", arr);
    do {
        swapped = false;
        for (let i = 0; i < arr.length - 1; i++) {
            console.log(`Comparing ${arr[i]} and ${arr[i + 1]}`);
            if (arr[i] > arr[i + 1]) {
                [arr[i], arr[i + 1]] = [arr[i + 1], arr[i]];
                swapped = true;
                console.log(`Swapped: ${arr[i]} and ${arr[i + 1]}, Array: `, arr);
                yield arr.slice(); // Yield the state of the array for visualization
            }
        }
    } while (swapped);
    console.log("Completed Bubble Sort: ", arr);
    return arr;
}

// Merge Sort Generator
export function* mergeSort(data, start = 0, end = data.length - 1) {
    if (start >= end) return; // Base case for recursion

    const middle = Math.floor((start + end) / 2);
    console.log(`Merge Sort split: ${start}-${middle} and ${middle+1}-${end}`);
    yield* mergeSort(data, start, middle);  // Sort the left half
    yield* mergeSort(data, middle + 1, end);  // Sort the right half
    yield* merge(data, start, middle, end);  // Merge the sorted halves
    return data;  // Return the final sorted array
}

function* merge(data, start, middle, end) {
    const left = data.slice(start, middle + 1);
    const right = data.slice(middle + 1, end + 1);
    let k = start, i = 0, j = 0;

    console.log(`Merging: `, left, right);
    while (i < left.length && j < right.length) {
        if (left[i] <= right[j]) {
            data[k++] = left[i++];
        } else {
            data[k++] = right[j++];
        }
        console.log(`Merging step: `, data);
        yield data.slice();
    }

    while (i < left.length) {
        data[k++] = left[i++];
        yield data.slice();
    }

    while (j < right.length) {
        data[k++] = right[j++];
        yield data.slice();
    }

    console.log(`Completed merge from ${start} to ${end}: `, data);
}

// Quick Sort Generator
export function* quickSort(data, start = 0, end = data.length - 1) {
    if (start >= end) return data;

    let index = yield* partition(data, start, end);
    console.log(`Quick Sort split at index ${index}:`, data);

    if (start < index - 1) {
        yield* quickSort(data, start, index - 1);
    }
    if (index < end) {
        yield* quickSort(data, index, end);
    }

    return data;
}

function* partition(data, start, end) {
    const pivotValue = data[Math.floor((start + end) / 2)];
    console.log(`Partitioning with pivot ${pivotValue} at index ${Math.floor((start + end) / 2)}`);
    let i = start, j = end;

    while (i <= j) {
        while (data[i] < pivotValue) {
            i++;
        }
        while (data[j] > pivotValue) {
            j--;
        }
        if (i <= j) {
            [data[i], data[j]] = [data[j], data[i]];
            console.log(`Swapped ${data[i]} and ${data[j]}:`, data);
            yield data.slice();  // Yield the state after each swap
            i++;
            j--;
        }
    }

    console.log(`Partition complete at index ${i}:`, data);
    return i;  // Return the index of the partition
}