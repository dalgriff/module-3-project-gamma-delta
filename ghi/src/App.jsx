
/**
 * The main component of the application.
 * Renders the navigation bar and routes to different components based on the current URL.
 *
 * @returns {JSX.Element} The rendered App component.
 */
import React, { useContext } from 'react';
import { Routes, Route } from 'react-router-dom';
import Navigation from './components/Navigation';
import { AuthContext } from './components/AuthProvider';
import SignInForm from './components/SignInForm';
import SignUpForm from './components/SignUpForm';
import Home from './components/Home';
import Demo from './components/Demo';
import './App.css';

const API_HOST = import.meta.env.VITE_API_HOST;
if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined');
}
function App() {
    const { isLoggedIn } = useContext(AuthContext); // Use authentication context

    return (
        <div>
            <Navigation />
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/signin" element={<SignInForm />} />
                <Route path="/signup" element={<SignUpForm />} />
                <Route path="/demo" element={isLoggedIn ? <Demo /> : <SignInForm />} />
            </Routes>
        </div>
    );
}

export default App;// // App.jsx
// import React from 'react';
// import { Routes, Route } from 'react-router-dom';
// import SignInForm from './components/SignInForm';
// import SignUpForm from './components/SignUpForm';
// import Home from './components/Home';  // Assuming you have a Home component
// import Demo from './components/Demo';  // Your sorting demo component

// function App() {
//     return (
//         <div>
//             <Routes>
//                 <Route path="/" element={<Home />} />
//                 <Route path="/signup" element={<SignUpForm />} />
//                 <Route path="/signin" element={<SignInForm />} />
//                 <Route path="/demo" element={<Demo />} />
//             </Routes>
//         </div>
//     );
// }

// export default App;


// import React from 'react';
// import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';

// // Import all necessary components
// import SignInForm from './components/SignInForm';
// import SignUpForm from './components/SignUpForm';
// import Home from './components/Home';
// import Demo from './components/Demo';

// // Main App component
// function App() {
//     return (
//         <Router>
//             <div>
//                 <nav>
//                     <ul>
//                         <li><Link to="/">Home</Link></li>
//                         <li><Link to="/signin">Sign In</Link></li>
//                         <li><Link to="/signup">Sign Up</Link></li>
//                         <li><Link to="/demo">Sorting Demo</Link></li>
//                     </ul>
//                 </nav>
//                 <Routes>
//                     <Route path="/" element={<Home />} />
//                     <Route path="/signin" element={<SignInForm />} />
//                     <Route path="/signup" element={<SignUpForm />} />
//                     <Route path="/demo" element={<Demo />} />
//                 </Routes>
//             </div>
//         </Router>
//     );
// }

// export default App;


// import React, { useState, useEffect, useCallback } from 'react';
// import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
// import SignInForm from './components/SignInForm';
// import SignUpForm from './components/SignUpForm';
// import Home from './components/Home';
// import Demo from './components/Demo';  // Consider wrapping your sorting logic in a Demo component

// import ErrorNotification from './components/ErrorNotification';
// import Construct from './components/Construct';
// import SortSelector from './components/SortSelector';
// import SortVisualizer from './components/SortVisualizer';
// import SortControls from './components/SortControls';
// import './App.css';

// const API_HOST = import.meta.env.VITE_API_HOST;
// if (!API_HOST) {
//     throw new Error('VITE_API_HOST is not defined');
// }

// function App() {
//     return (
//         <Router>
//             <div>
//                 <nav>
//                     <Link to="/">Home</Link>
//                     <Link to="/signinform">Sign In</Link>
//                     <Link to="/signupform">Sign Up</Link>
//                     <Link to="/demo">Sorting Demo</Link>
//                 </nav>
//                 <Routes>
//                     <Route path="/" element={<Home />} />
//                     <Route path="/signinform" element={<SignInForm />} />
//                     <Route path="/signupform" element={<SignUpForm />} />
//                     <Route path="/demo" element={<Demo />} />
//                 </Routes>
//             </div>
//         </Router>
//     );
// }

// export default App;






// // Import necessary React components and styles
// import { useState, useEffect, useCallback } from 'react';
// import { Outlet } from 'react-router-dom';
// import ErrorNotification from './components/ErrorNotification';
// import Construct from './components/Construct';
// import SortSelector from './components/SortSelector';
// import SortVisualizer from './components/SortVisualizer';
// import SortControls from './components/SortControls';
// import './App.css';

// const API_HOST = import.meta.env.VITE_API_HOST;
// if (!API_HOST) {
//     throw new Error('VITE_API_HOST is not defined');
// }

// // Generator functions for sorting algorithms
// import { bubbleSort, mergeSort, quickSort } from './utils/sortingGenerators';

// function App() {
//     const [launchInfo, setLaunchInfo] = useState();
//     const [error, setError] = useState(null);
//     const [data, setData] = useState([]);
//     const [generator, setGenerator] = useState(null);

//     // Fetch initial data on component mount
//     useEffect(() => {
//         const fetchData = async () => {
//             try {
//                 const response = await fetch(`${API_HOST}/api/launch-details`);
//                 const result = await response.json();
//                 if (!response.ok) throw new Error(result.message || 'Failed to load launch details');
//                 setLaunchInfo(result.launch_details);
//                 setData(result.sort_data || []);
//             } catch (err) {
//                 console.error('Error fetching launch data:', err);
//                 setError(err.message);
//             }
//         };
//         fetchData();
//     }, []);

//     const handleAlgorithmChange = useCallback((algorithm) => {
//         const sortedData = [...data]; // Clone data to ensure immutability
//         let gen = null;
//         switch (algorithm) {
//             case 'Bubble Sort':
//                 gen = bubbleSort(sortedData);
//                 break;
//             case 'Quick Sort':
//                 gen = quickSort(sortedData);
//                 break;
//             case 'Merge Sort':
//                 gen = mergeSort(sortedData);
//                 break;
//             default:
//                 return;
//         }
//         setGenerator(gen);
//         stepThroughSort(gen);
//     }, [data]);

//     const stepThroughSort = useCallback((gen) => {
//         if (!gen) return;
//         const result = gen.next();
//         console.log('Sorting step:', result.value);  // Log each step for educational purposes
//         if (!result.done) {
//             setData(result.value);
//         } else {
//             setGenerator(null);  // Cleanup after sorting completion
//         }
//     }, []);

//     const handleDatasetChange = async (selectedDataset) => {
//         try {
//             const response = await fetch(`${API_HOST}/data/${selectedDataset}`);
//             const newData = await response.json();
//             if (!Array.isArray(newData)) throw new Error('Data fetched is not an array');
//             setData(newData);
//         } catch (error) {
//             console.error('Error fetching new dataset:', error);
//             setError(error.message);
//             setData([]);
//         }
//     };

//     return (
//         <div className="App">
//             <Outlet />
//             <ErrorNotification error={error} />
//             <Construct info={launchInfo} />
//             <SortSelector onAlgorithmChange={handleAlgorithmChange} onDatasetChange={handleDatasetChange} algorithms={['Bubble Sort', 'Quick Sort', 'Merge Sort']} datasets={['randomNumbers', 'sortedNumbers', 'reverseNumbers']} />
//             <SortVisualizer data={data} />
//             {generator && <SortControls onNext={() => stepThroughSort(generator)} />}
//         </div>
//     );
// }

// export default App;