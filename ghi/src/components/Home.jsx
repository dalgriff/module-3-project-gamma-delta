/**
 * Renders the Home component.
 * 
 * This component displays a welcome message and conditionally renders a link to the demo page
 * based on the user's authentication status.
 * 
 * @returns {JSX.Element} The rendered Home component.
 */
import React from 'react';
import { Link } from 'react-router-dom';
import useAuthService from '../hooks/useAuthService';  // Importing the custom hook


function Home() {
    const { isLoggedIn } = useAuthService();  // Using the custom hook to access auth state

    return (
        <div>
            <h1>Welcome to Our Sorting Algorithm Demo</h1>
            {isLoggedIn ? (
                <Link to="/demo">Go to Demo</Link>
            ) : (
                <div>
                    <p>Please <Link to="/signin">Sign In</Link> or <Link to="/signup">Sign Up</Link> to view the demo.</p>
                </div>
            )}
        </div>
    );
}

export default Home;