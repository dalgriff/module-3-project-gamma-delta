import React from 'react';

const SortSelector = ({ onAlgorithmChange, onDatasetChange, algorithms, datasets }) => {
  return (
    <div>
      <select onChange={e => onAlgorithmChange(e.target.value)}>
        {algorithms.map(algo => (
          <option key={algo} value={algo}>{algo}</option>
        ))}
      </select>
      <select onChange={e => onDatasetChange(e.target.value)}>
        {datasets.map(data => (
          <option key={data} value={data}>{data}</option>
        ))}
      </select>
    </div>
  );
};

export default SortSelector;