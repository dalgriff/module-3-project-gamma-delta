import React from 'react';

const SortVisualizer = ({ data }) => {
  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'end', height: '200px', gap: '4px' }}>
      {data.map((value, index) => (
        <div key={index} style={{ width: '20px', height: `${value}px`, backgroundColor: 'dodgerblue' }}></div>
      ))}
    </div>
  );
};

export default SortVisualizer;