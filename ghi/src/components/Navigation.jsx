import React from 'react';
import { Link } from 'react-router-dom';
import useAuthService from '../hooks/useAuthService';



function Navigation() {
    const { isLoggedIn, user, signout } = useAuthService();

    const handleLogout = async () => {
        await signout();
    };
    return (
        <nav>
            <ul className="navbar">
                <li><Link to="/">Home</Link></li>
                {!isLoggedIn ? (
                    // If the user is not logged in, display the Sign In and Sign Up links
                    <>
                        <li><Link to="/signin">Sign In</Link></li>
                        <li><Link to="/signup">Sign Up</Link></li>
                    </>
                ) : (
                    // If the user is logged in, display the Demo link, user's username, and Logout button
                    <>
                        <li><Link to="/demo">Demo</Link></li>
                        <li>Howdy, {user?.username}</li>
                        <li><button onClick={handleLogout}>Logout</button></li>
                    </>
                )}
            </ul>
        </nav>
    );
}

export default Navigation;