/**
 * Demo component for sorting visualization.
 *
 * This component fetches launch details and sort data from an API and displays them.
 * It allows the user to select different sorting algorithms and datasets to visualize the sorting process.
 *
 * @component
 * @example
 * return (
 *   <Demo />
 * )
 */
import React, { useState, useEffect, useCallback } from 'react';
import ErrorNotification from './ErrorNotification';
import SortSelector from './SortSelector';
import SortVisualizer from './SortVisualizer';
import SortControls from './SortControls';
import { bubbleSort, mergeSort, quickSort } from '../utils/sortingGenerators';


const API_HOST = import.meta.env.VITE_API_HOST;
if (!API_HOST) {
    throw new Error('VITE_API_HOST is not defined');
}

function Demo() {
    const [launchInfo, setLaunchInfo] = useState({});
    const [error, setError] = useState(null);
    const [data, setData] = useState([]);
    const [generator, setGenerator] = useState(null);
    const [selectedAlgorithm, setSelectedAlgorithm] = useState('Bubble Sort');

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(`${API_HOST}/api/launch-details`);
                const result = await response.json();
                if (!response.ok) {
                    throw new Error(result.message || 'Failed to load launch details');
                }
                setLaunchInfo(result.launch_details);
                setData(result.sort_data || []);
            } catch (err) {
                console.error('Error fetching launch data:', err);
                setError(err.message);
            }
        };
        fetchData();
        handleAlgorithmChange('SelectedAlgorithm');
    }, []);

    const handleAlgorithmChange = useCallback((algorithm) => {
        setSelectedAlgorithm(algorithm);
        const sortedData = [...data];
        let gen = null;
        switch (algorithm) {
            case 'Bubble Sort':
                gen = bubbleSort(sortedData);
                break;
            case 'Quick Sort':
                gen = quickSort(sortedData);
                break;
            case 'Merge Sort':
                gen = mergeSort(sortedData);
                break;
            default:
                return;
        }
        setGenerator(gen);
        stepThroughSort(gen);
    }, [data]);

    const stepThroughSort = useCallback((gen) => {
        if (!gen) return;
        const result = gen.next();
        if (!result.done) {
            setData(result.value);
        } else {
            setGenerator(null); // Cleanup after sorting completion
        }
    }, []);

    const handleDatasetChange = async (selectedDataset) => {
        try {
            const response = await fetch(`${API_HOST}/data/${selectedDataset}`);
            const newData = await response.json();
            if (!Array.isArray(newData)) throw new Error('Data fetched is not an array');
            setData(newData);
            handleAlgorithmChange(selectedAlgorithm);
        } catch (error) {
            console.error('Error fetching new dataset:', error);
            setError(error.message);
            setData([]);
        }
    };

    return (
        <div className="demo-page">
            <ErrorNotification error={error} />
            <SortSelector onAlgorithmChange={handleAlgorithmChange} onDatasetChange={handleDatasetChange} algorithms={['Bubble Sort', 'Quick Sort', 'Merge Sort']} datasets={['randomNumbers', 'sortedNumbers', 'reverseNumbers']} />
            <SortVisualizer data={data} />
            {generator && <SortControls onNext={() => stepThroughSort(generator)} />}
        </div>
    );
}

export default Demo;