function SortControls({ onStart, onNext }) {
    return (
        <div>
            <button onClick={onStart}>Start Sorting</button>
            <button onClick={onNext}>Next Step</button>
        </div>
    );
}
export default SortControls;