/**
 * Main entry point of the application.
 * Renders the root component wrapped in necessary providers and routers.
 * @module Main
 */
//@ts-check
import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';  // Use BrowserRouter here
import App from './App';
import AuthProvider from './components/AuthProvider';
import './index.css';

const rootElement = document.getElementById('root');
if (!rootElement) {
    throw new Error('root element was not found!');
}

const root = ReactDOM.createRoot(rootElement);
root.render(
    <React.StrictMode>
        <AuthProvider>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </AuthProvider>
    </React.StrictMode>
);